package main

import (
	"bufio"
	"io"
	"log"
	"os"
)

const (
    DefaultSliceCap = 1e3
)

// Convenience function to open and return file.
func fileOpener(fp string) *os.File {
    outf, err := os.Open(fp)
    if err != nil {
        log.Fatal(err)
    }
    return outf
}

// Retrieve data from specified file path.
func GetDataFrom(filePath string) []string {
    // Open file and defer closing under operations are complete.
    openedFile := fileOpener(filePath)
    defer openedFile.Close()

    // Slice for capturing and returning data.
    lineSlice := make([]string, 0, DefaultSliceCap)

    // Scan each line until the end
    scanner := newScanner(openedFile)
    for scanner.Scan() {
        // Append new data to slice.
        lineSlice = append(lineSlice, scanner.Text())
    }

    // Return data.
    return lineSlice
}

// Write data to specified file path.
func WriteDataTo(filePath string, data []string) {
    outf, err := os.Create(filePath)
    if err != nil {
        log.Fatal(err)
    }
    defer outf.Close()

    for _, line := range data {
        _, err := outf.WriteString(line + "\n")
        if err != nil {
            log.Fatal(err)
        }
    }
}

func MaybeClearFolder(folderPath string) {
	files, err := os.ReadDir(folderPath)
	if err != nil {
		log.Fatal(err)
	}
    // Remove any files present.
    for _, f := range files {
        if f.IsDir() {
            continue
        }
        fName := f.Name()
        if fName[len(fName)-3:] == "txt" {
            os.Remove(folderPath + "/" + fName)
        }
    }
}

// Return new scanner for provided Reader object.
func newScanner(reader io.Reader) *bufio.Scanner {
    return bufio.NewScanner(reader)
}
