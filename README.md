# Northern Llight Sharepoint Project

### To run:

1. Update `./data/input/` folder with file content.  
  a. preferably replacing what is in 1.txt and 2.txt.  
1. Try running the following from a terminal: `$ ./bin/solution.exe`  
  a. if you encounter issues and have Go installed locally, you can also try running:  `$ ./scripts/makke-and-run/sh`  
  b. Without compiling, you can run: `$ go run .`

(**Note**: the commands might be different on Windows; This was build on a Linux machine.)

For any other questions, please feel free to reach out and I try to respond as promptly as possible.
