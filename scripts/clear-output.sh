#!/bin/bash
set -e

# Clear output folder.

: ${OUTPUT_DATA_FOLDER:=data/output}

[[ -d $OUTPUT_DATA_FOLDER ]] && rm "${OUTPUT_DATA_FOLDER}"/*.txt
