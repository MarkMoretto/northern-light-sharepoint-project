#!/bin/bash
set -e

# Build and run executable.

: ${PATH_TO_EXE:=bin/solution.exe}

go build -ldflags="-s -w" -o "${PATH_TO_EXE}" && ./"${PATH_TO_EXE}"
