package main

// Program to read contents of two files, then create two new files which
// each contain contents of each file that are unique to each file.

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
)

// Paths to files and folders with data for reading and writing.
const (
	RootDataFolder     = `./data`
	PathToInputFolder  = RootDataFolder + `/input`
	PathToOutputFolder = RootDataFolder + `/output`
	// Prefix for output file.  A numeric value will be added
	// after the hyphen.
	OutputFilePrefix = `unique-`
)

func main() {
	defer writer.Flush()
	var (
		inputData, outputData [2][]string
		destFilePaths         [2]string
		ffp                   string
	)
	// Initialize data objects.
	for i := range inputData {
		inputData[i] = []string{}
	}

	// Create string slices and output file name slice.
	for i := range outputData {
		outputData[i] = make([]string, 0, DefaultSliceCap)
		destFilePaths[i] = fmt.Sprintf("%s/%s%d.txt", PathToOutputFolder, OutputFilePrefix, i+1)
	}

	// Clear output folder
    MaybeClearFolder(PathToOutputFolder)

	// Begin file operations.
	files, err := os.ReadDir(PathToInputFolder)
	if err != nil {
		log.Fatal(err)
	}

	for i, f := range files {
		// Create full file path and validate path.
		ffp = PathToInputFolder + "/" + f.Name()

		// Check validity of path.
		if _, err := os.Stat(ffp); err != nil {
			log.Fatal(err)
		}

		inputData[i] = GetDataFrom(ffp)
	}

	ii := 1
	for i := range inputData {
        // Switch indexes.
		ii ^= i
		src, cmp := inputData[i], inputData[ii]
		for _, w := range src {
			if Some(cmp, func(x string) bool { return x == w }) {
				continue
			}
			outputData[i] = append(outputData[i], w)
		}
	}

	// Iterate result data
	// Ensure lexicon sort
	// Write to destionation file.
	for i, d := range outputData {
		sort.Strings(d)
		WriteDataTo(destFilePaths[i], d)
	}
}

// Check `items` slice for any valid func() results.
func Some(items []string, f func(string) bool) bool {
	for _, ele := range items {
		if f(ele) {
			return true
		}
	}
	return false
}

// IO Buffers
var (
	writer *bufio.Writer = bufio.NewWriter(os.Stdout)
)

func Printf(f string, a ...interface{}) {
	fmt.Fprintf(writer, f, a...)
}
